import {assert} from "chai";
import {Claim} from "./claim";

describe("claims", () => {
    let c = new Claim();
    it("should not expire by default", () => {
        assert.isFalse(c.expires());
    });
    it("should not have a user id by default", () => {
        assert.isFalse(c.hasUserId());
    });
    it("should be able to configure expiration", () => {
        c.expiresAt = 123;
        assert.isTrue(c.expires());
    });
    it("should be able to configure a user id", () => {
        c.userMetadata.userId = "abc";
        assert.isTrue(c.hasUserId());
    });
});
