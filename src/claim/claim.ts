export class Claim {
    /**
     * The audience of this claim.
     */
    public audience: string;
    public expiresAt: number;

    /**
     * An indication whether this claim expires or not.
     * @returns {boolean} Does this claim expire?
     */
    public expires(): boolean {
        return this.expiresAt !== -1;
    }
    public issuedAt: number;
    public issuer: string;
    public subject: string;
    public userMetadata: {
        companyId: string,
        userId: string,
        domain: string,
    };
    public hasUserId(): boolean {
        return this.userMetadata.userId !== "";
    }
    public scopes: string[];

    constructor() {
        this.expiresAt = -1;

        this.userMetadata = {
            companyId: "",
            userId: "",
            domain: "",
        };
    }
}
