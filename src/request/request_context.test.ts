import {assert} from "chai";
import {RequestContext, MiddlewareRegistratorContext, MiddlewareRequestContext, newRequestContext} from "./request_context";
import {newTestRequestContextBuilder} from "../testrequest/builder";

describe("RequestContext", () => {
    describe("#getRequestHeader", () => {
        describe("withoutRequest", () => {
            let context = newRequestContext();
            let header = context.getRequestIDHeader();
            it("should return the header from the request", () => {
                assert.isNotNull(header);
            });
            it("the request id should have a value", () => {
                assert.notEqual("", header.value);
            });
            it("should have an empty href", () => {
                assert.equal("", context.href());
            });
        });
    });
    describe("as middleware", () => {
        it("should execute the chain in FIFO order.", (done) => {
            var ctx = newRequestContext();
            var mwreg = <MiddlewareRegistratorContext>ctx;

            let mwctx = <MiddlewareRequestContext>ctx;

            var str = "";

            mwreg.registerMiddleware((ctx: MiddlewareRequestContext) => {
                str += "|!|";
                assert.equal("|the||correct||order||!|", str);
                ctx.next(); // Should have no effect.
                done();
            });
            mwreg.registerMiddleware((ctx: MiddlewareRequestContext) => {
                str += "|order|";
                ctx.next();
            });
            mwreg.registerMiddleware((ctx: MiddlewareRequestContext) => {
                str += "|correct|";
                ctx.next();
            });
            mwreg.registerMiddleware((ctx: MiddlewareRequestContext) => {
                str += "|the|";
                ctx.next();
            });
            // Start
            mwctx.next();
        });
        it("should allow partial failures.", (done) => {
            var ctx = newRequestContext();
            var mwreg = <MiddlewareRegistratorContext>ctx;

            let mwctx = <MiddlewareRequestContext>ctx;

            mwreg.registerMiddleware((ctx: MiddlewareRequestContext) => {
                done(new Error("I should not be called"));
            });
            mwreg.registerMiddleware((ctx: MiddlewareRequestContext) => {
                ctx.stop();
                setTimeout(done, 25);
            });
            mwctx.next();
        });
        it("should fail on errors in next.", (done) => {
            var ctx = newTestRequestContextBuilder(() => {
                assert.isTrue(ctx.testResponse().committed());
                assert.deepEqual(ctx.testResponse().getResponse(), {
                    code: "InternalServerError",
                    message: "abc"
                });
                assert.equal(ctx.testResponse().getStatus(), 500);
                done();
            }).build();
            var mwreg = <MiddlewareRegistratorContext>ctx;

            let mwctx = <MiddlewareRequestContext>ctx;

            mwreg.registerMiddleware((c: MiddlewareRequestContext) => {
                done(new Error("I should not be called"));
            });
            mwreg.registerMiddleware((c: MiddlewareRequestContext) => {
                c.next(new Error("abc"));
            });
            mwctx.next();
        });
        it("MW Errors should be caught.", (done) => {
            var ctx = newTestRequestContextBuilder(() => {
                assert.isTrue(ctx.testResponse().committed());
                assert.deepEqual(ctx.testResponse().getResponse(), {
                    code: "InternalServerError",
                    message: "I should be caught"
                });
                assert.equal(ctx.testResponse().getStatus(), 500);
                done();
            }).build();
            var mwreg = <MiddlewareRegistratorContext>ctx;

            let mwctx = <MiddlewareRequestContext>ctx;

            mwreg.registerMiddleware((c: MiddlewareRequestContext) => {
                done(new Error("I should not be called"));
            });
            mwreg.registerMiddleware((c: MiddlewareRequestContext) => {
                throw new Error("I should be caught");
            });
            mwctx.next();
        });

        it("should return the token string", () => {
            let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX21ldGFkYX";
            var ctx = newTestRequestContextBuilder().withHeader("Authorization", "Bearer " + token).build();
            assert.equal(ctx.getToken(), token);                             
        });
    });
});
