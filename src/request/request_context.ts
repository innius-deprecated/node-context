import * as uuid from "node-uuid";

import {Context} from "./../Context";
import {Header} from "./../Header";
import {Logger, newScopedLogger} from "@toincrease/node-logger";
import {Request, Response, Next} from "restify";
import {TaskContext} from "./../task_context";
import {ResponseContext, ResContext} from "../response/response_context";
import {Claim} from "../claim/claim";

export interface RequestContext extends Context {
    request(): Request;

    getLogLevelHeader(): Header;
    getToken(): string;
    /**
     * Retrieve the claim associated with this request, if any. 
     * @returns {Claim} The claim for this request.
     */
    claim(): Claim;
    /**
     * Does this request context have any claims associated with it?
     * @returns boolean Indication on whether claims exist.
     */
    hasClaim(): boolean;

    /**
     * Retrieve the request id header.
     * @returns {}
     */
    getRequestIDHeader(): Header;

    response(): ResponseContext;

    deriveTaskContext(): TaskContext;

    // Request access
    /**
     * Get a header from a request context.
     * @param key The key of the header.
     * @param defaultValue An optional default value of the header.
     * @returns {Header} The header with the found value, or the optional default one.
     */
    header(key: string, defaultValue?: string): Header;
    contentLength(): number;
    contentType(): string;
    href(): string;
    path(): string;
    query(param: string): string;
    params(param: string): string;
    body(): any;
}

export interface MutableRequestContext extends RequestContext {
    setClaim(claim: Claim): void;
}

export interface MiddlewareRequestContext extends MutableRequestContext {
    /**
     * Invoke the next middleware func, or the actual handler if this was the last one.
     * @param {any} err An optional HTTP error of the failure.
     */
    next(err?: any): void;
    /**
     * Stop processing this chain.
     */
    stop(): void;
}

export interface MiddlewareRegistratorContext extends MutableRequestContext {
    /**
     * Register a new middleware func for this request. Middleware is executed in a FIFO order.
     * @param f The middleware function.
     */
    registerMiddleware(f: (ctx: MiddlewareRequestContext) => void): void;
}

const requestid_header = "X-Request-Id";
const loglevel_header = "X-Log-Level";
const loglevel_header_default_value = "info";
const authorization_header = "Authorization";

export function newRequestContext(req?: Request, res?: Response, n?: Next, log?: Logger): RequestContext {
    return new ReqContext(req, res, n, log);
}

class ReqContext implements RequestContext, MutableRequestContext, MiddlewareRegistratorContext, MiddlewareRequestContext {
    private _claim: Claim;
    private log: Logger;
    private resctx: ResponseContext;
    private handlers: ((ctx: MiddlewareRequestContext) => void)[];

    constructor(private req?: Request, res?: Response, n?: Next, log?: Logger) {
        if (!log) {
            this.log = newScopedLogger();
        } else {
            this.log = log;
        }
        this._claim = undefined;
        this.resctx = new ResContext(this, res, n);
        this.handlers = [];
    };

    // Middleware related ops
    registerMiddleware(f: (ctx: MiddlewareRequestContext) => void): void {
        this.handlers.push(f);
    }

    next(err?: any): void {
        if (err) {
            this.response().error(err);
        } else {
            let f = this.handlers.pop();
            if (f) {
                try {
                    f(this);
                } catch (e) {
                    this.response().error(e);
                }
            }
        }
    }

    stop(): void {
        this.handlers = [];
        return;
    }

    // Normal request ops

    claim(): Claim {
        return this._claim;
    }

    hasClaim(): boolean {
        return this.claim() !== undefined;
    }

    setClaim(claim: Claim): void {
        this._claim = claim;
        this.log = this.log.withFields(this._claim.userMetadata);
    }

    request(): Request {
        return this.req;
    }

    response(): ResponseContext {
        return this.resctx;
    }

    logger(): Logger {
        return this.log;
    };

    header(key: string, defaultValue?: string): Header {
        if (this.hasRequestContext()) {
            return <Header>{
                name: key,
                value: this.request().header(key, defaultValue)
            };
        }
        return <Header>{
            name: key,
            value: defaultValue
        };
    }

    getRequestIDHeader(): Header {
        return this.header(requestid_header, uuid.v4());
    }

    getLogLevelHeader(): Header {
        return this.header(loglevel_header, loglevel_header_default_value);
    };

    getToken(): string {
        if (this.hasRequestContext()) {
            let ah: string = this.header(authorization_header, "").value;
            if (ah.length > 6 && (ah.toUpperCase().substr(0, 6)) === "BEARER") {
                return ah.substr(7);
            }            
        };
        return "";
    }

    deriveTaskContext(): TaskContext {
        return this;
    }

    contentLength(): number {
        if (!this.hasRequestContext()) {
            return -1;
        }
        return this.request().contentLength;
    }

    contentType(): string {
        if (!this.hasRequestContext()) {
            return "";
        }
        return this.request().contentType;
    }

    href(): string {
        if (!this.hasRequestContext()) {
            return "";
        }
        return this.request().href();
    }

    path(): string {
        if (!this.hasRequestContext()) {
            return "";
        }
        return this.request().path();
    }

    query(param: string): string {
        if (!this.hasRequestContext()) {
            return "";
        }
        return this.request().query[param];
    }

    params(param: string): string {
        if (!this.hasRequestContext()) {
            return "";
        }
        return this.request().params[param];
    }

    body(): any {
        if (!this.hasRequestContext()) {
            return null;
        }
        return this.request().body;
    }

    private hasRequestContext(): boolean {
        return this.request() !== undefined;
    }
}
