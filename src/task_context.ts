import {Context} from "./Context";
import {Logger, newScopedLogger} from "@toincrease/node-logger";

export function newTaskContext(properties: any): TaskContext {
    return new TaskContextImpl(properties);
}

export interface TaskContext extends Context {

}

export class TaskContextImpl implements TaskContext {
    private log: Logger;

    constructor(properties: any) {
        this.log = newScopedLogger(properties);
    };

    logger(): Logger {
        return this.log;
    };
}
