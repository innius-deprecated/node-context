import {Context} from "./../Context";
import {Header} from "./../Header";
import {Logger} from "@toincrease/node-logger";
import {RequestContext} from "../request/request_context";
import {Response, Next} from "restify";
import {toHTTPError} from "../errors/errors";

export interface ResponseContext extends Context {
    /**
     * Is the response already committed? Can only be done once.
     */
    committed(): boolean;
    // Responses
    /**
     * Send a JSON response.
     * @param body The body to send.
     * @param status An optional status code.
     * @param headers Optional headers.
     */
    json(body: any, status?: number, headers?: Header[]): void;
    /**
     * Send a response.
     * @param body The body to send.
     * @param status An optional status code.
     * @param headers Optional headers.
     */
    send(body: string, status?: number, headers?: Header[]): void;
    /**
     * Send an error.
     * @param error The error to send.
     */
    error(error: any): void;
    /**
     * Invoke the next handler directly, optionally by providing an error.
     * This does NOT send any data back to the caller, 
     * @param err
     */
}

function mapHeaders(headers: Header[]): { [header: string]: string; } {
    let _headers: { [header: string]: string; } = {};
    if (!headers) {
        return _headers;
    }
    for (var h of headers) {
        _headers[h.name] = h.value;
    }
    return _headers;
}

export class ResContext implements ResponseContext {
    private _committed: boolean;
    private log: Logger;
    private req: RequestContext;
    private res: Response;
    private next: Next;

    constructor(request: RequestContext, response?: Response, n?: Next) {
        this.log = request.logger();
        this.req = request;
        this.res = response;
        this.next = n;
        this._committed = false;
    };

    logger(): Logger {
        return this.log;
    }

    json(body: any, status?: number, headers?: Header[]): void {
        if (this.committed()) {
            this.log.error("Response already committed.");
            return;
        }
        this.commit();
        if (!status) {
            status = 200;
        }
        this.res.json(status, body, mapHeaders(headers));
        this.next();
    }

    send(body: string, status?: number, headers?: Header[]): void {
        if (this.committed()) {
            this.log.error("Response already committed.");
            return;
        }
        this.commit();
        if (!status) {
            status = 200;
        }
        this.res.send(status, body, mapHeaders(headers));
        this.next();
    }

    error(error: any): void {
        this.log.error(error);
        if (this.committed()) {
            this.log.error("Response already committed.");
            return;
        }
        this.commit();
        var e = toHTTPError(error);
        this.res.json(e.statusCode, e.body);
    }

    committed(): boolean {
        return this._committed;
    }

    private commit(): void {
        this._committed = true;
    }
}
