import {ResponseContext} from "./response_context";
import {Header} from "../header";
import {Logger} from "@toincrease/node-logger";
import {Next} from "restify";
import {toHTTPError} from "../errors/errors";
import {Response} from "restify";

export class TestResponseContext implements ResponseContext {
    private respBody: any;
    private respNumber: number;
    private respHeaders: Header[];
    private _committed: boolean;
    private next: Next;

    constructor(private rctx: ResponseContext, n: Next) {
        this._committed = false;
        this.next = n;
    }

    getResponse(): any {
        return this.respBody;
    }

    getStatus(): number {
        return this.respNumber;
    }

    getHeaders(): Header[] {
        return this.respHeaders;
    }

    committed(): boolean {
        return this._committed;
    }

    nextFunc(): Next {
        return this.next;
    }

    /**
     * Get the raw response object that has the resp args swapped again.
     * Use this to reflect the restify api.
     * Note: this is a very limited Response object.
     * @returns {Response} The raw response object.
     */
    raw_response(): Response {
        var that = this;
        var r = {
            json: undefined,
            send: undefined
        };
        r.json = (status: number, body: any, headers: any) => {
            that.json(body, status, headers);
        };
        r.send = (status: number, body: any, headers: any) => {
            that.send(body, status, headers);
        };
        return <Response>r;
    }

    private process(body: any, status?: number, headers?: Header[]): void {
        if (this.committed()) {
            return;
        }
        if (!status) {
            status = 200;
        }
        this._committed = true;
        this.respBody = body;
        this.respNumber = status;
        this.respHeaders = headers;
        this.next();
    }

    json(body: any, status?: number, headers?: Header[]): void {
        this.process(body, status, headers);
    }

    send(body: string, status?: number, headers?: Header[]): void {
        this.process(body, status, headers);
    }

    error(error: any): void {
        var e = toHTTPError(error);
        this.process(e.body, e.statusCode, []);
    }

    logger(): Logger {
        return this.rctx.logger();
    }

}
