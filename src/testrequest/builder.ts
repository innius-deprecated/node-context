import {newRequestContext, MutableRequestContext} from "./../request/request_context";
import {TestRequestOptions, createTestRequest} from "./test_request";

import {newScopedLogger} from "@toincrease/node-logger";

import {Request, Response, Next} from "restify";
import {Claim} from "../claim/claim";
import {TestRequestContext} from "./test_request_context";

/**
 * Implement te Next interface on a simple callback.
 * @param cb
 * @returns {Next}
 */
function next(cb: (err?: any) => void): Next {
    let f: any = (err?: any) => cb(err);
    f.ifError = (err?: any) => cb(err);
    return <Next>f;
}

/**
 * Create a new test request context object.
 */
export function newTestRequestContextBuilder(cb?: (err?: any) => void): TestRequestContextBuilder {
    if (!cb) {
        cb = () => "";
    }
    return new TestRequestContextBuilder(cb);
}

export class TestRequestContextBuilder {
    private reqOpts: TestRequestOptions;
    private claim: Claim;

    /**
     * @param cb An optional callback (Next()) that is called when the request is completed.
     */
    constructor(private cb: (err?: any) => void) {
        this.reqOpts = new TestRequestOptions();
        this.claim = undefined;
    }

    /**
     * Build a new test request context object.
     * @returns {TestRequestContext} The test request context.
     */
    build(): TestRequestContext {
        let req: Request = createTestRequest(this.reqOpts);
        let res: Response = <Response>{};
        let n: Next = next(this.cb);
        let log = newScopedLogger();
        let rctx = newRequestContext(req, res, n, log);
        let mut = <MutableRequestContext>rctx;
        mut.setClaim(this.claim);
        return new TestRequestContext(rctx, res, n);
    };

    /**
     * Set a Next() middleware func (can be used as a callback for completion) in a fluent manner.
     * @param cb The func to configure. Only one can be set.
     * @return {TestRequestContextBuilder} This request context builder.
     */
    withNext(cb: (err?: any) => void): TestRequestContextBuilder {
        this.cb = cb;
        return this;
    }

    /**
     * Set a callback. Alias of withNext().
     * @param cb The callback to configure. Only one can be set.
     * @return {TestRequestContextBuilder} This request context builder.
     */
    withCallback(cb: (err?: any) => void): TestRequestContextBuilder {
        return this.withNext(cb);
    }

    /**
     * Add a parameter to the test request.
     * @param p The param name to add.
     * @param val The param value to add.
     * @returns {TestRequestContextBuilder} This request context builder.
     */
    withParam(p: string, val: any): TestRequestContextBuilder {
        this.reqOpts.params[p] = val;
        return this;
    };

    /**
     * Define the method to use for this request.
     * @param method The HTTP Method to use.
     * @returns {TestRequestContextBuilder} This request context builder.
     */
    withMethod(method: string): TestRequestContextBuilder {
        this.reqOpts.method = method;
        return this;
    }

    /**
     * Define the URL for this request.
     * @param url The URL.
     * @returns {TestRequestContextBuilder} This request context builder.
     */
    withUrl(url: string): TestRequestContextBuilder {
        this.reqOpts.url = url;
        return this;
    }

    /**
     * Define the original url for this request.
     * @param url The URL
     * @returns {TestRequestContextBuilder} This request context builder.
     */
    withOriginalUrl(url: string): TestRequestContextBuilder {
        this.reqOpts.originalUrl = url;
        return this;
    }

    /**
     * Define the path for this request.
     * @param path The path.
     * @returns {TestRequestContextBuilder} This request context builder.
     */
    withPath(path: string): TestRequestContextBuilder {
        this.reqOpts.path = path;
        return this;
    }

    /**
     * Define a header for this request.
     * @param key A header key.
     * @param value A header value.
     * @returns {TestRequestContextBuilder} This request context builder.
     */
    withHeader(key: string, value: string): TestRequestContextBuilder {
        this.reqOpts.headers[key] = value;
        return this;
    }

    /**
     * Define a query param.
     * @param key The query param key.
     * @param value The query param value.
     * @returns {TestRequestContextBuilder} This request context builder.
     */
    withQuery(key: string, value: string): TestRequestContextBuilder {
        this.reqOpts.query[key] = value;
        return this;
    }

    /**
     * Define a request body.
     * @param body The request body.
     * @returns {TestRequestContextBuilder} This request context builder.
     */
    withBody(body: any): TestRequestContextBuilder {
        this.reqOpts.body = body;
        return this;
    }

    /**
     * Define a claim that is to be set on the request context object for this test request.
     * @param claim The claim to set.
     * @returns {TestRequestContextBuilder} This request content builder.
     */
    withClaim(claim: Claim): TestRequestContextBuilder {
        this.claim = claim;
        return this;
    }
}
