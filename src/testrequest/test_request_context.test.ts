import {assert} from "chai";
import {newTestRequestContextBuilder} from "./builder";
import {NotFoundError} from "restify";

describe("TestRequestContext", () => {
    it("Should call next only once.", () => {
        var next = 0;
        let context = newTestRequestContextBuilder(() => {
            next++;
        }).build();
        assert.isFalse(context.response().committed());

        context.response().send("Hello, world");
        assert.isTrue(context.response().committed());
        assert.equal(1, next);

        context.response().send("This can't have any effect.");
        assert.equal(1, next);
    });
    it("Should be able to read the response.", () => {
        var next = 0;
        let context = newTestRequestContextBuilder(() => {
            next++;
        }).build();
        assert.isFalse(context.response().committed());

        context.response().send("Hello, world");
        assert.equal("Hello, world", context.testResponse().getResponse());
    });
    it("Should be able to cope with errors", () => {
        var e = new NotFoundError("abc");
        let context = newTestRequestContextBuilder().build();
        context.response().error(e);
        assert.deepEqual({
            code: "NotFoundError",
            message: "abc"
        }, context.testResponse().getResponse());
        assert.equal(404, context.testResponse().getStatus());
    });
    it("Should be able to cope with normal errors", () => {
        var e = new Error("abc");
        let context = newTestRequestContextBuilder().build();
        context.response().error(e);
        assert.deepEqual({
            code: "InternalServerError",
            message: "abc"
        }, context.testResponse().getResponse());
        assert.equal(500, context.testResponse().getStatus());
    });
    it("Should be able to set a next func in a fluent manner", (done) => {
        let context = newTestRequestContextBuilder().withNext(done).build();
        context.response().send("");
    });
    it("should be able to use withCallback as an alias of withNext()", (done) => {
        let context = newTestRequestContextBuilder().withCallback(done).build();
        context.response().send("");
    });
    it("Should be able to use a request context builder", () => {
        let b = {
            "a": "b",
            "c": [
                "d",
                "e"
            ]
        };
        let builder = newTestRequestContextBuilder();
        let ctx = builder
            .withParam("foo", "bar")
            .withHeader("abc", "def")
            .withBody(b)
            .build();
        assert.equal(ctx.params("foo"), "bar");
        let h = ctx.header("abc");
        assert.equal(h.name, "abc");
        assert.equal(h.value, "def");
        assert.equal(ctx.body(), b);
        console.log(ctx.href());
    });
    it("Should have the href() shorthand", () => {
        let ctx = newTestRequestContextBuilder().build();
        assert.equal(ctx.href(), "");
    });
    it("Should be able to set a correct response.", () => {
        let ctx = newTestRequestContextBuilder().build();
        ctx.response().json("body", 201);
        assert.equal(ctx.testResponse().getResponse(), "body");
        assert.equal(ctx.testResponse().getStatus(), 201);
    });
    it("should have status 200 by default", () => {
        let ctx = newTestRequestContextBuilder().build();
        ctx.response().send("abc");
        assert.equal(ctx.testResponse().getResponse(), "abc");
        assert.equal(ctx.testResponse().getStatus(), 200);
    });
});
