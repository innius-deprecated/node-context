import {Request} from "restify";
import {parse} from "url";
var httpMocks = require("node-mocks-http"); // no typings available

export class TestRequestOptions {
    method: string = "GET";
    url: string = "";
    originalUrl: string = this.url;
    path: string = "";
    params: any = {};
    session: any = undefined;
    cookies: any = {};
    signedCookies: any = undefined;
    headers: any = {};
    body: any = {};
    query: any = {};
    files: any = {};
}

export function createTestRequest(options: TestRequestOptions): Request {
    var r = httpMocks.createRequest(options);
    r["href"] = () => parse(options.url).href;
    return r;
}
