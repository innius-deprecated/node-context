import {
    RequestContext, MutableRequestContext,
    MiddlewareRegistratorContext, MiddlewareRequestContext
} from "./../request/request_context";

import {Header} from "../header";
import {ResponseContext, ResContext} from "../response/response_context";
import {TaskContext} from "../task_context";
import {TestResponseContext} from "../response/test_response_context";

import {Logger} from "@toincrease/node-logger";

import {Request, Response, Next} from "restify";
import {Claim} from "../claim/claim";

/**
 * Add a transformation layer so that the hack below (see 'circular') can work as expected.
 * Possibly TODO: transform headers. 
 * @param f Our handler io func
 * @returns {function(number=, any=, any=): void} a 'transformed' handler func.
 */
function transform(f: (body?: any, status?: number, headers?: Header[]) => void): (body: any, status: number, headers: Header[]) => void {
    return (status?: number, body?: any, headers?: any): void => f(body, status, headers);
}

export class TestRequestContext implements RequestContext, MutableRequestContext, MiddlewareRegistratorContext, MiddlewareRequestContext {
    private resp: ResponseContext;

    constructor(private rctx: RequestContext, r: Response, n: Next) {
        this.resp = new TestResponseContext(new ResContext(this, r, n), n);
        // Create a circular reference for testing purposes.
        r.send = transform(this.response().send.bind(this.response()));
        r.json = transform(this.response().json.bind(this.response()));
    }

    claim(): Claim {
        return this.rctx.claim();
    }

    hasClaim(): boolean {
        return this.rctx.hasClaim();
    }

    setClaim(claim: Claim): void {
        let c = <MutableRequestContext>this.rctx;
        c.setClaim(claim);
    }

    registerMiddleware(f: (ctx: MiddlewareRequestContext) => void): void {
        let c = <MiddlewareRegistratorContext>this.rctx;
        c.registerMiddleware(f);
    }

    /**
     * Only applicable if cast as interface MiddlewareRequestContext
     * @param err An optional error.
     */
    next(err?: any): void {
        let c = <MiddlewareRequestContext>this.rctx;
        c.next(err);
    }

    stop(): void {
        let c = <MiddlewareRequestContext>this.rctx;
        c.stop();
    }

    // Additional stuff
    testResponse(): TestResponseContext {
        return <TestResponseContext>this.resp;
    }

    // Overridden behaviour

    response(): ResponseContext {
        return this.resp;
    }

    // Simple parent invocations

    request(): Request {
        return this.rctx.request();
    }

    getLogLevelHeader(): Header {
        return this.rctx.getLogLevelHeader();
    }

    getToken(): string {
        return this.rctx.getToken();
    }

    getRequestIDHeader(): Header {
        return this.rctx.getRequestIDHeader();
    }

    deriveTaskContext(): TaskContext {
        return this.rctx.deriveTaskContext();
    }

    /**
     * Get a header from a request context.
     * @param key The key of the header.
     * @param defaultValue An optional default value of the header.
     * @returns {Header} The header with the found value, or the optional default one.
     */
    header(key: string, defaultValue?: string): Header {
        return this.rctx.header(key, defaultValue);
    }

    contentLength(): number {
        return this.rctx.contentLength();
    }

    contentType(): string {
        return this.rctx.contentType();
    }

    href(): string {
        return this.rctx.href();
    }

    path(): string {
        return this.rctx.path();
    }

    query(param: string): string {
        return this.rctx.query(param);
    }

    params(param: string): string {
        return this.rctx.params(param);
    }

    body(): any {
        return this.rctx.body();
    }

    logger(): Logger {
        return this.rctx.logger();
    }

}
