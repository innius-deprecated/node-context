import {Logger} from "@toincrease/node-logger";

export interface Context {
    logger(): Logger;
}
