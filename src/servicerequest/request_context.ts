import {Logger} from "@toincrease/node-logger";
import {RequestContext} from "../request/request_context";
import {Header} from "./../Header";
import {Request} from "restify";
import {Claim} from "../claim/claim";
import {ResponseContext} from "../response/response_context";
import {TaskContext} from "./../task_context";

class ServiceRequestContext implements RequestContext {
    private _context: RequestContext;

    constructor(c: RequestContext, private token: string) {
        this._context = c;
    }

    request(): Request {
        return this._context.request();
    }

    getLogLevelHeader(): Header {
        return this._context.getLogLevelHeader();
    }
    
    getToken(): string {
        return this.token;
    }

    claim(): Claim {
        return this._context.claim();
    }

    hasClaim(): boolean {
        return this._context.hasClaim();
    }

    getRequestIDHeader(): Header {
        return this._context.getRequestIDHeader();
    }

    response(): ResponseContext {
        return this._context.response();
    }

    deriveTaskContext(): TaskContext {
        return this._context.deriveTaskContext();
    }

    header(key: string, defaultValue?: string): Header {
        return this._context.header(key, defaultValue);
    }
    
    contentLength(): number {
        return this._context.contentLength();
    }
    
    contentType(): string {
        return this._context.contentType();
    }
    
    href(): string {
        return this._context.href();
    }
    
    path(): string {
        return this._context.path();
    }

    query(param: string): string {
        return this._context.query(param);
    }
    
    params(param: string): string {
        return this._context.params(param);
    }
    
    body(): any {
        return this._context.body();
    }
    
    logger(): Logger {
        return this._context.logger();
    };
}

export function newServiceRequestContext(c: RequestContext, token: string): RequestContext {
    return new ServiceRequestContext(c, token);
}
