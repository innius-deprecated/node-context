import {assert} from "chai";
import {newServiceRequestContext} from "./request_context";
import {newTestRequestContextBuilder} from "../testrequest/builder";

describe("ServiceRequestContext", () => {
    describe("new service context with token", () => {
        let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX21ldGFkYX";
        var ctx = newTestRequestContextBuilder()            
            	.withHeader("Authorization", "Bearer " + token)
                .withHeader("X-Log-Level", "debug")
                .withHeader("X-Request-Id", "plantage")
                .build();
                
        let svctoken = "service_token";
        let sctx = newServiceRequestContext(ctx, svctoken);
        
        it("should return the service token", () => {
            assert.equal(sctx.getToken(), svctoken);
        });    
        it("context should have loglevel of source request", () => {            
            assert.equal(sctx.getLogLevelHeader().value, "debug");
        });
        it("context should have conversation-id of the source request", () => {
            assert.equal(sctx.getRequestIDHeader().value, "plantage");
        });        
    });
});
