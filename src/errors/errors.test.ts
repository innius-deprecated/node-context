import {assert} from "chai";
import {toHTTPError} from "./errors";
import {NotFoundError} from "./export";

describe("errors", () => {
    describe("A generic error", () => {
        var _e = new Error("abc");
        var e = toHTTPError(_e);
        it("should have status code 500", () => {
            assert.equal(500, e.statusCode);
        });
        it("should be an internal server error.", () => {
            assert.equal("abc", e.message);
        });
        it("should have a body.", () => {
            assert.equal("InternalServerError", e.body.code);
            assert.equal("abc", e.body.message);
        });
    });
    describe("A typed error", () => {
        var _e = new NotFoundError("xyz");
        var e = toHTTPError(_e);
        it("should have status code 404", () => {
            assert.equal(404, e.statusCode);
        });
        it("should be an internal server error.", () => {
            assert.equal("xyz", e.message);
        });
        it("should have a body.", () => {
            assert.equal("NotFoundError", e.body.code);
            assert.equal("xyz", e.body.message);
        });
    });
    describe("A malformed error", () => {
        var _e = new NotFoundError("xyz");
        _e["body"] = undefined;
        it("should cope with malformed errors", (done) => {
            var e = toHTTPError(_e);
            assert.equal(500, e.statusCode);
            assert.equal("{\"message\":\"xyz\",\"statusCode\":404}", e.message);
            assert.equal("InternalServerError", e.body.code);
            assert.equal("{\"message\":\"xyz\",\"statusCode\":404}", e.body.message);
            done();
        });
    });
});
