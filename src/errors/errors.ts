import {InternalServerError} from "./export";

export interface HttpErrorBody {
    code: string;
    message: string;
}

export interface HttpError {
    message: string;
    statusCode: number;
    body: HttpErrorBody;
}

export function toHTTPError(e: any): HttpError {
    if (e.constructor.name === "Error") {
        return toHTTPError(new InternalServerError(e.message));
    } else if (e.constructor.name.indexOf("Error") > -1 || e.constructor.name === "") {
        try {
            var _e = <any>e;
            return <HttpError>{
                message: _e.message,
                statusCode: _e.statusCode,
                body: <HttpErrorBody>{
                    code: _e.body.code,
                    message: _e.body.message,
                }
            };
        } catch (_) {
            return toHTTPError(new InternalServerError(JSON.stringify(e)));
        }
    } else {
        return toHTTPError(new InternalServerError(JSON.stringify(e)));
    }
}
